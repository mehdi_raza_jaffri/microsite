<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('articles.article'));
});

Route::get('/article', 'ArticleController@articles')->name('articles.article');

Route::get('/article/{slug}', 'ArticleController@articleBySlug');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/upload', 'ArticleController@upload')->name('upload');

Route::post('/storeJson', 'ArticleController@storeJson')->name('articles.storeJson');

Route::resource('images', 'ImageController');

Route::resource('authors', 'AuthorController');

Route::resource('articles', 'ArticleController');

Route::resource('chapters', 'ChapterController');
