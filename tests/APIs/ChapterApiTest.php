<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeChapterTrait;
use Tests\ApiTestTrait;

class ChapterApiTest extends TestCase
{
    use MakeChapterTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_chapter()
    {
        $chapter = $this->fakeChapterData();
        $this->response = $this->json('POST', '/api/chapters', $chapter);

        $this->assertApiResponse($chapter);
    }

    /**
     * @test
     */
    public function test_read_chapter()
    {
        $chapter = $this->makeChapter();
        $this->response = $this->json('GET', '/api/chapters/'.$chapter->id);

        $this->assertApiResponse($chapter->toArray());
    }

    /**
     * @test
     */
    public function test_update_chapter()
    {
        $chapter = $this->makeChapter();
        $editedChapter = $this->fakeChapterData();

        $this->response = $this->json('PUT', '/api/chapters/'.$chapter->id, $editedChapter);

        $this->assertApiResponse($editedChapter);
    }

    /**
     * @test
     */
    public function test_delete_chapter()
    {
        $chapter = $this->makeChapter();
        $this->response = $this->json('DELETE', '/api/chapters/'.$chapter->id);

        $this->assertApiSuccess();
        $this->response = $this->json('GET', '/api/chapters/'.$chapter->id);

        $this->response->assertStatus(404);
    }
}
