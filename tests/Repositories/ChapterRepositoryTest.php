<?php namespace Tests\Repositories;

use App\Models\Chapter;
use App\Repositories\ChapterRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\Traits\MakeChapterTrait;
use Tests\ApiTestTrait;

class ChapterRepositoryTest extends TestCase
{
    use MakeChapterTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ChapterRepository
     */
    protected $chapterRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->chapterRepo = \App::make(ChapterRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_chapter()
    {
        $chapter = $this->fakeChapterData();
        $createdChapter = $this->chapterRepo->create($chapter);
        $createdChapter = $createdChapter->toArray();
        $this->assertArrayHasKey('id', $createdChapter);
        $this->assertNotNull($createdChapter['id'], 'Created Chapter must have id specified');
        $this->assertNotNull(Chapter::find($createdChapter['id']), 'Chapter with given id must be in DB');
        $this->assertModelData($chapter, $createdChapter);
    }

    /**
     * @test read
     */
    public function test_read_chapter()
    {
        $chapter = $this->makeChapter();
        $dbChapter = $this->chapterRepo->find($chapter->id);
        $dbChapter = $dbChapter->toArray();
        $this->assertModelData($chapter->toArray(), $dbChapter);
    }

    /**
     * @test update
     */
    public function test_update_chapter()
    {
        $chapter = $this->makeChapter();
        $fakeChapter = $this->fakeChapterData();
        $updatedChapter = $this->chapterRepo->update($fakeChapter, $chapter->id);
        $this->assertModelData($fakeChapter, $updatedChapter->toArray());
        $dbChapter = $this->chapterRepo->find($chapter->id);
        $this->assertModelData($fakeChapter, $dbChapter->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_chapter()
    {
        $chapter = $this->makeChapter();
        $resp = $this->chapterRepo->delete($chapter->id);
        $this->assertTrue($resp);
        $this->assertNull(Chapter::find($chapter->id), 'Chapter should not exist in DB');
    }
}
