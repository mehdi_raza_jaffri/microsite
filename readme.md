## Chip MicroSite

This Site Contains Articles

/article - This page will display all the articles published.
/upload - This page will be used to Upload Article JSON
/article/{url_slug} - This route will get user to the main published article. 

## How to Setup

- You should have docker and docker-compose installed in your machine
- Run docker-compose up
- then Run  docker-compose exec php_api php artisan migrate:fresh

make sure you have set the correct docker db credentials in the .env file at the root.

_DB_CONNECTION=mysql
DB_HOST=db_api // here using container name
DB_PORT=3306
DB_DATABASE=articles
DB_USERNAME=chip
DB_PASSWORD=chip_


## How Docker is setup

In docker-compose nginx , php and mysql images are pulled and being used.

## Play around

First go to /upload page and upload the json, if json is valid it will redirect you 
to the /article page


## Contact

- +923341257992
- skype: mehdiraza93