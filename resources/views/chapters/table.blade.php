<div class="table-responsive">
    <table class="table" id="chapters-table">
        <thead>
            <tr>
                <th>Order</th>
        <th>Headline</th>
        <th>Text</th>
        <th>Image</th>
        <th>Article</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($chapters as $chapter)
            <tr>
                <td>{!! $chapter->order !!}</td>
            <td>{!! $chapter->headline !!}</td>
            <td>{!! $chapter->text !!}</td>
            <td>{!! $chapter->image !!}</td>
            <td>{!! $chapter->article !!}</td>
                <td>
                    {!! Form::open(['route' => ['chapters.destroy', $chapter->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('chapters.show', [$chapter->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('chapters.edit', [$chapter->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
