<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $chapter->id !!}</p>
</div>

<!-- Order Field -->
<div class="form-group">
    {!! Form::label('order', 'Order:') !!}
    <p>{!! $chapter->order !!}</p>
</div>

<!-- Headline Field -->
<div class="form-group">
    {!! Form::label('headline', 'Headline:') !!}
    <p>{!! $chapter->headline !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $chapter->text !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $chapter->image !!}</p>
</div>

<!-- Article Field -->
<div class="form-group">
    {!! Form::label('article', 'Article:') !!}
    <p>{!! $chapter->article !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $chapter->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $chapter->updated_at !!}</p>
</div>

