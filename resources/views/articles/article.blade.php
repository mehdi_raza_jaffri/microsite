<html lang="en-US"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>Micro Site</title>

    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>

<body>
<section class="blog-post style-three pad-100">
    <div>
        <div>
            <div>
                @foreach($articles as $article)
                    <article class="has-animation animate-in" data-delay="0">
                        <div class="entry-meta-content">
                            <h2 class="entry-title"><span>{!! $article->headline[0] !!}</span><a href="#">{!! $article->headline !!}</a></h2>
                            <span>  by:  {!! $article->authors->first_name !!}</span>
                        </div>
                        <div class="entry-content-bottom">
                            <p class="entry-content">{!! $article->introduction !!}</p>
                            <a href="/article/{{ $article->url_slug }}" class="entry-read-more"><span></span>Read More</a>
                        </div>
                    </article>
                @endforeach
            </div>
        </div>
    </div>
</section>

<!-- SCRIPTS -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>