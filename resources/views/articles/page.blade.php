<!DOCTYPE HTML>
<html>
<head>
    <title>{!! $article->headline !!}</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="{{asset("css/_inner.css")}}" />
</head>
<body>


<section id="main" class="wrapper">
    <div class="container">

        <header class="major">
            <h2>{!! $article->headline !!}</h2>
            <p>{!! $article->subtitle !!}</p>
            <p style="align-content: flex-end">By   {!! $article->authors->first_name !!}</p>
        </header>


        <a href="#" class="image fit"><img src="{!! $article->images->url !!}" alt="" /></a>
        <p>
            {!! $article->introduction !!}
        </p>

    </div>
</section>

@foreach($article->chapters as $chapter)
    <section id="main" class="wrapper">
        <div class="container">

            <header class="major">
                <h2>{!! $chapter->headline !!}</h2>
                <p>{!! $chapter->subtitle !!}</p>
            </header>

            <a href="#" class="image fit"><img src="{!! $chapter->images['url'] !!}" alt="" /></a>
            <p>
                {!! $chapter->text !!}
            </p>

        </div>
    </section>
@endforeach

<section>

<h2 style="text-align:center">Images</h2>
    <div class="row">
        @foreach($article->chapters as $key => $chapter)
            @if($chapter->image)
                <div class="column">
                    <img src="{!! $chapter->images['url'] !!}" style="width:100%" onclick="openModal();currentSlide({!! $key !!})" class="hover-shadow cursor">
                </div>
            @endif
        @endforeach
    </div>

    <div id="myModal" class="modal">
        <span class="close cursor" onclick="closeModal()">&times;</span>
        <div class="modal-content">

            @foreach($article->chapters as $key => $chapter)
                @if($chapter->images)
                <div class="mySlides">
                    <img src="{!! $chapter->images['url'] !!}" style="width:100%">
                </div>
                @endif
            @endforeach

            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>

            <div class="caption-container">
                <p id="caption"></p>
            </div>

            @foreach($article->chapters as $key => $chapter)
                    @if($chapter->images)
                        <div class="column">
                            <img src="{!! $chapter->images['url'] !!}" style="width:100%" onclick="openModal();currentSlide({!! $key !!})" class="hover-shadow cursor">
                        </div>
                    @endif
            @endforeach
        </div>
    </div>

</section>
<script src="{{ asset('js/app.js')}}"></script>
</body>
</html>