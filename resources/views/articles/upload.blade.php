<body>
    <section>
        <h1>
            Upload Json Article File
        </h1>
    </section>
    <div>
        <div>

            <div>
                <div>
                    @include('vendor/flash/message')
                    {!! Form::open(['route' => 'articles.storeJson', 'files' => true]) !!}

                    <div>
                        {!! Form::label('file', 'File:') !!}
                        {!! Form::file('json_file', null) !!}
                    </div>

                    <br>

                    <div>
                        {!! Form::submit('Upload') !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</body>