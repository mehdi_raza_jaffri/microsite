<!-- Url Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url_id', 'Url Id:') !!}
    {!! Form::number('url_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Url Slug Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url_slug', 'Url Slug:') !!}
    {!! Form::text('url_slug', null, ['class' => 'form-control']) !!}
</div>

<!-- Headline Field -->
<div class="form-group col-sm-6">
    {!! Form::label('headline', 'Headline:') !!}
    {!! Form::text('headline', null, ['class' => 'form-control']) !!}
</div>

<!-- Subtitle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtitle', 'Subtitle:') !!}
    {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
</div>

<!-- Introduction Field -->
<div class="form-group col-sm-6">
    {!! Form::label('introduction', 'Introduction:') !!}
    {!! Form::text('introduction', null, ['class' => 'form-control']) !!}
</div>

<!-- Display Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('display_date', 'Display Date:') !!}
    {!! Form::text('display_date', null, ['class' => 'form-control']) !!}
</div>

<!-- Author Field -->
<div class="form-group col-sm-6">
    {!! Form::label('author', 'Author:') !!}
    {!! Form::text('author', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', 'Image:') !!}
    {!! Form::text('image', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('articles.index') !!}" class="btn btn-default">Cancel</a>
</div>
