<div class="table-responsive">
    <table class="table" id="articles-table">
        <thead>
            <tr>
                <th>Url Id</th>
        <th>Url Slug</th>
        <th>Headline</th>
        <th>Subtitle</th>
        <th>Introduction</th>
        <th>Display Date</th>
        <th>Author</th>
        <th>Image</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
            <tr>
                <td>{!! $article->url_id !!}</td>
            <td>{!! $article->url_slug !!}</td>
            <td>{!! $article->headline !!}</td>
            <td>{!! $article->subtitle !!}</td>
            <td>{!! $article->introduction !!}</td>
            <td>{!! $article->display_date !!}</td>
            <td>{!! $article->author !!}</td>
            <td>{!! $article->image !!}</td>
                <td>
                    {!! Form::open(['route' => ['articles.destroy', $article->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('articles.show', [$article->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('articles.edit', [$article->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
