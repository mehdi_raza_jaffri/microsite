<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $article->id !!}</p>
</div>

<!-- Url Id Field -->
<div class="form-group">
    {!! Form::label('url_id', 'Url Id:') !!}
    <p>{!! $article->url_id !!}</p>
</div>

<!-- Url Slug Field -->
<div class="form-group">
    {!! Form::label('url_slug', 'Url Slug:') !!}
    <p>{!! $article->url_slug !!}</p>
</div>

<!-- Headline Field -->
<div class="form-group">
    {!! Form::label('headline', 'Headline:') !!}
    <p>{!! $article->headline !!}</p>
</div>

<!-- Subtitle Field -->
<div class="form-group">
    {!! Form::label('subtitle', 'Subtitle:') !!}
    <p>{!! $article->subtitle !!}</p>
</div>

<!-- Introduction Field -->
<div class="form-group">
    {!! Form::label('introduction', 'Introduction:') !!}
    <p>{!! $article->introduction !!}</p>
</div>

<!-- Display Date Field -->
<div class="form-group">
    {!! Form::label('display_date', 'Display Date:') !!}
    <p>{!! $article->display_date !!}</p>
</div>

<!-- Author Field -->
<div class="form-group">
    {!! Form::label('author', 'Author:') !!}
    <p>{!! $article->author !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Image:') !!}
    <p>{!! $article->image !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $article->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $article->updated_at !!}</p>
</div>

