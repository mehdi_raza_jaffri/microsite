<?php

namespace App\Repositories;

use App\Models\Chapter;
use App\Repositories\BaseRepository;

/**
 * Class ChapterRepository
 * @package App\Repositories
 * @version July 21, 2019, 8:37 am UTC
*/

class ChapterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'order',
        'headline',
        'text'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Chapter::class;
    }
}
