<?php

namespace App\Repositories;

use App\Models\Article;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ArticleRepository
 * @package App\Repositories
 * @version July 21, 2019, 8:33 am UTC
 */

class ArticleRepositoryInfoym extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'url_id',
        'url_slug',
        'headline',
        'subtitle',
        'introduction',
        'display_date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Article::class;
    }
}
