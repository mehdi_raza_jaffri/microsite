<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateChapterAPIRequest;
use App\Http\Requests\API\UpdateChapterAPIRequest;
use App\Models\Chapter;
use App\Repositories\ChapterRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ChapterController
 * @package App\Http\Controllers\API
 */

class ChapterAPIController extends AppBaseController
{
    /** @var  ChapterRepository */
    private $chapterRepository;

    public function __construct(ChapterRepository $chapterRepo)
    {
        $this->chapterRepository = $chapterRepo;
    }

    /**
     * Display a listing of the Chapter.
     * GET|HEAD /chapters
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $chapters = $this->chapterRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($chapters->toArray(), 'Chapters retrieved successfully');
    }

    /**
     * Store a newly created Chapter in storage.
     * POST /chapters
     *
     * @param CreateChapterAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateChapterAPIRequest $request)
    {
        $input = $request->all();

        $chapter = $this->chapterRepository->create($input);

        return $this->sendResponse($chapter->toArray(), 'Chapter saved successfully');
    }

    /**
     * Display the specified Chapter.
     * GET|HEAD /chapters/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Chapter $chapter */
        $chapter = $this->chapterRepository->find($id);

        if (empty($chapter)) {
            return $this->sendError('Chapter not found');
        }

        return $this->sendResponse($chapter->toArray(), 'Chapter retrieved successfully');
    }

    /**
     * Update the specified Chapter in storage.
     * PUT/PATCH /chapters/{id}
     *
     * @param int $id
     * @param UpdateChapterAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateChapterAPIRequest $request)
    {
        $input = $request->all();

        /** @var Chapter $chapter */
        $chapter = $this->chapterRepository->find($id);

        if (empty($chapter)) {
            return $this->sendError('Chapter not found');
        }

        $chapter = $this->chapterRepository->update($input, $id);

        return $this->sendResponse($chapter->toArray(), 'Chapter updated successfully');
    }

    /**
     * Remove the specified Chapter from storage.
     * DELETE /chapters/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Chapter $chapter */
        $chapter = $this->chapterRepository->find($id);

        if (empty($chapter)) {
            return $this->sendError('Chapter not found');
        }

        $chapter->delete();

        return $this->sendResponse($id, 'Chapter deleted successfully');
    }
}
