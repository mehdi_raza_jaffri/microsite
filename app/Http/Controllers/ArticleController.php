<?php

namespace App\Http\Controllers;


use App\Http\Requests\CreateArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Models\Article;
use App\Repositories\ArticleRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ArticleRepositoryInfoym;
use App\Repositories\AuthorRepository;
use App\Repositories\ChapterRepository;
use App\Repositories\ImageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Flash;
use Response;
use DateTime;

class ArticleController extends AppBaseController
{
    /** @var  ArticleRepository */
    private $articleRepository;
    private $articleRepositoryInfoym;
    private $imageRepository;
    private $authorRepository;
    private $chapterRepository;

    public function __construct(ArticleRepository $articleRepo, ArticleRepositoryInfoym $articleRepositoryInfoym, ImageRepository $imageRepository, ChapterRepository $chapterRepository, AuthorRepository $authorRepository)
    {
        $this->articleRepository = $articleRepo;
        $this->imageRepository = $imageRepository;
        $this->authorRepository = $authorRepository;
        $this->chapterRepository = $chapterRepository;
        $this->articleRepositoryInfoym = $articleRepositoryInfoym;
    }

    /**
     * Display a listing of the Article.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $articles = $this->articleRepository->all();

        return view('articles.index')
            ->with('articles', $articles);
    }


    public function articles(Request $request)
    {
        $articles = $this->articleRepositoryInfoym->with(['images','authors'])->all();
        return view('articles.article')
            ->with('articles', $articles);
    }

    /**
     * Show the form for creating a new Article.
     *
     * @return Response
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * Show the form for upload a new Article.
     *
     * @return Response
     */
    public function upload()
    {
        return view('articles.upload');
    }

    /**
     * Store a newly created Article in storage.
     *
     * @param CreateArticleRequest $request
     *
     * @return Response
     */
    public function store(CreateArticleRequest $request)
    {

        $input = $request->all();

        $article = $this->articleRepository->create($input);

        Flash::success('Article saved successfully.');

        return redirect(route('articles.index'));
    }

    /**
     * Store a newly created Article in storage.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function storeJson(Request $request)
    {
        try{
            $input = $request->all();

            $strJsonFileContents = file_get_contents($input['json_file']);
            $data = json_decode($strJsonFileContents);

            if($data){

                // Create Image
                $image = $this->imageRepository->create($data->image);

                // Create Author
                $authorData = $data->author;
                $author = $this->authorRepository->create(['first_name' => $authorData->firstName , 'last_name' => $authorData->lastName]);


                // Prepare data for Article
                $timeStamp = $data->displayDate->timestamp;
                $dt = new DateTime("@$timeStamp");

                $articleData = [
                    'image' => $image->id,
                    'author' => $author->id,
                    'url_id' => $data->urlId,
                    'url_slug' => $data->urlSlug,
                    'headline' => $data->headline,
                    'subtitle' => $data->subtitle,
                    'display_date' => $dt->format('Y-m-d H:i:s'),
                    'introduction' => $data->introduction
                ];

                $article = $this->articleRepository->create($articleData);

                if(!empty($data->chapters)){
                    $chapters = $data->chapters;
                    foreach ($chapters as $chapter){
                        if(count((array)$chapter->image)){
                            $image = $this->imageRepository->create($chapter->image);
                            $chapter->image = $image->id;
                        }else{
                            unset($chapter->image);
                        }
                        $chapter->article = $article->id;
                        $this->chapterRepository->create($chapter);
                    }
                }

                return redirect(route('articles.article'));

            }else{
                Flash::error('Invalid JSON File');
                return redirect(route('upload'));
            }

        } catch (\Exception $exception){
            Flash::error($exception);
            return redirect(route('upload'));
        }

    }

    /**
     * Display the specified Article.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
//      $article = $this->articleRepositoryInfoym->with(['images','authors'])->find($id);
        $article = $this->articleRepositoryInfoym->with(['images','authors'])->findWhere(['url_id'=> $id])->get(1);
        if (empty($article)) {
            Flash::error('Article not found');

            return redirect(route('articles.index'));
        }
        return view('articles.show')->with('article', $article);
    }


    public function articleBySlug($slug)
    {
        $article = $this->articleRepositoryInfoym->with(['images','authors','chapters', 'chapters.images'])->findWhere(['url_slug'=> $slug]);

        if ($article->count() <= 0) {
            abort(404);
        }
//        echo $article[0];
        return view('articles.page')->with('article', $article[0]);
    }

    /**
     * Show the form for editing the specified Article.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            Flash::error('Article not found');

            return redirect(route('articles.index'));
        }

        return view('articles.edit')->with('article', $article);
    }

    /**
     * Update the specified Article in storage.
     *
     * @param int $id
     * @param UpdateArticleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArticleRequest $request)
    {
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            Flash::error('Article not found');

            return redirect(route('articles.index'));
        }

        $article = $this->articleRepository->update($request->all(), $id);

        Flash::success('Article updated successfully.');

        return redirect(route('articles.index'));
    }

    /**
     * Remove the specified Article from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $article = $this->articleRepository->find($id);

        if (empty($article)) {
            Flash::error('Article not found');

            return redirect(route('articles.index'));
        }

        $this->articleRepository->delete($id);

        Flash::success('Article deleted successfully.');

        return redirect(route('articles.index'));
    }
}
