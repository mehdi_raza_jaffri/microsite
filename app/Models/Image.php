<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Image
 * @package App\Models
 * @version July 21, 2019, 8:33 am UTC
 *
 * @property integer height
 * @property integer width
 * @property string text
 * @property string url
 * @property string source
 */
class Image extends Model
{
    use SoftDeletes;

    public $table = 'images';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'height',
        'width',
        'text',
        'url',
        'source'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'height' => 'integer',
        'width' => 'integer',
        'text' => 'string',
        'url' => 'string',
        'source' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'height' => 'required',
        'width' => 'required',
        'text' => 'required',
        'url' => 'required',
        'source' => 'required'
    ];
}
