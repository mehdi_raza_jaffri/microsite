<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Chapter
 * @package App\Models
 * @version July 21, 2019, 8:37 am UTC
 *
 * @property \App\Models\Image image
 * @property \App\Models\Article article
 * @property integer order
 * @property string headline
 * @property string text
 */
class Chapter extends Model
{
    use SoftDeletes;

    public $table = 'chapters';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'order',
        'headline',
        'text',
        'image',
        'article'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'order' => 'integer',
        'headline' => 'string',
        'text' => 'string',
        'image' => 'integer',
        'article' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'order' => 'required',
        'headline' => 'required',
        'text' => 'required',
        'image' => 'required',
        'article' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function images()
    {
        return $this->belongsTo(\App\Models\Image::class,'image','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function article()
    {
        return $this->belongsTo(\App\Models\Article::class);
    }
}
