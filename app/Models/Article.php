<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Article
 * @package App\Models
 * @version July 21, 2019, 8:33 am UTC
 *
 * @property \App\Models\Author author
 * @property \App\Models\Image image
 * @property integer url_id
 * @property string url_slug
 * @property string headline
 * @property string subtitle
 * @property string introduction
 */
class Article extends Model
{
    use SoftDeletes;

    public $table = 'articles';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'url_id',
        'url_slug',
        'headline',
        'subtitle',
        'introduction',
        'author',
        'image',
        'display_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'url_id' => 'integer',
        'url_slug' => 'string',
        'headline' => 'string',
        'subtitle' => 'string',
        'introduction' => 'string',
        'author' => 'integer',
        'image' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'url_id' => 'required',
        'url_slug' => 'required',
        'headline' => 'required',
        'subtitle' => 'required',
        'introduction' => 'required',
        'author' => 'required',
        'image' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function authors()
    {
        return $this->belongsTo(\App\Models\Author::class, 'author','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function images()
    {
        return $this->belongsTo(\App\Models\Image::class, 'image', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function chapters()
    {
        return $this->hasMany(\App\Models\Chapter::class, 'article', 'id');
    }
}
