<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateArticlesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('url_id')->unsigned();
            $table->string('url_slug', 100);
            $table->string('headline');
            $table->string('subtitle');
            $table->string('introduction',1000);
            $table->timestamp('display_date');
            $table->integer('author')->unsigned();
            $table->integer('image')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('author')->references('id')->on('authors');
            $table->foreign('image')->references('id')->on('images');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
