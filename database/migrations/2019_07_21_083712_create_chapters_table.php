<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChaptersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order')->unsigned();
            $table->string('headline');
            $table->string('text',15000);
            $table->integer('image')->unsigned()->nullable();
            $table->integer('article')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('image')->references('id')->on('images');
            $table->foreign('article')->references('id')->on('articles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chapters');
    }
}
